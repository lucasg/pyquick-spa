#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands

"""
Copy Special exercise

"""

#
# +++your code here+++
#
# Escribi funciones y modifica el main() para llamarlas.


def main():
    # Este parseo de linea de comandos esta provisto.
    # Agrega codigo para llamar a tus funciones abajo.

    # Hacer una lista con los argumentos, omitiendo el elemento [0]
    # el cual es el nombre del mismo script.
    args = sys.argv[1:]
    if not args:
        print "usage: [--todir dir][--tozip zipfile] dir [dir ...]"
        sys.exit(1)

    # todir y tozip son un set proveniente de la linea de comandos o
    # bien dejados como una string vacia.
    # El array args queda contieniendo solo los dirs
    todir = ''
    if args[0] == '--todir':
        todir = args[1]
        del args[0:2]

    tozip = ''
    if args[0] == '--tozip':
        tozip = args[1]
        del args[0:2]

    if len(args) == 0:
        print "error: must specify one or more dirs"
        sys.exit(1)

    #
    # +++your code here+++
    #
    # Llama a tus funciones

if __name__ == "__main__":
    main()
