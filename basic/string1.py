#!/usr/bin/python -tt

# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/


# Ejercicios basicos de strings.
# Rellenar el codigo faltante en las funciones de abajo.
# La funcion main() ya esta seteada para llamar a las funciones
# con algunos argumentos diferentes, imprimiendo OK para las
# funciones correctas.
#
# Run Tests: python string1.py


# A. donuts
#
# Dada la cantidad de donuts, devolver una string
# de la forma 'Number of donuts: <count>, donde <count> es el
# numero pasado a la funcion. Pero si la cantidad es >= 10 usar
# la palabra 'many' en vez de el valor de <count>.
#
# So donuts(5) returns 'Number of donuts: 5'
# and donuts(23) returns 'Number of donuts: many'
def donuts(count):
    # +++your code here+++
    return


# B. both_ends
#
# Dada una string s, devolver una string que sea la
# concatenacion de los primeros 2 y los ultimos 2 caracteres.
# Pero si la string es de largo menor a 2, devolver la string
# vacia en vez.
#
# So both_ends('dinosaur') returns 'diur'
# and both_ends('apple') return 'aple'
def both_ends(s):
    # +++your code here+++
    return


# C. fix_start
#
# Dada una string s, devolver una string donde todas las
# ocurrencias de su primer caracter hayan sido reemplazadas
# por '*', exepto por el primer caracter.
# Asumir que la string es de largo >= 1.
# Pista: s.replace(stra, strb) returns una version de string s
# donde todas las instacias de stra an sido reemplazadas por strb.
#
# So fix_start('babble') return 'ba**le'
# and fix_start('anana') returns 'an*n*'
def fix_start(s):
    # +++your code here+++
    return


# D. MixUp
#
# Dada strings a and b devolver un sola string con a and b separadas
# por un espacio '<a> <b>' e intercambiar los 2 primeros chars de cada
# string. Asumir que a and b son de largo >= 2.
#
# So mix_up('mix', pod') -> 'pox mid'
# and mix_up('dog', 'dinner') -> 'dig donner'
def mix_up(a, b):
    # +++your code here+++
    return



# Provided simple test() function used in main() to print
# what each function returns vs. what it's supposed to return.
def test(got,expected):
    if got == expected:
        prefix = ' OK '
    else:
        prefix= '  X '
        print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


# Provided main() calls the above functions with interesting inputs,
# using test() to check if each result is correct or not.
def main():
    print 'donuts'
    # Each line calls donuts, compares its result to the expected for that call.
    test(donuts(4), 'Number of donuts: 4')
    test(donuts(9), 'Number of donuts: 9')
    test(donuts(10), 'Number of donuts: many')
    test(donuts (99), 'Number of donuts: many')

    print
    print 'both_ends'
    test(both_ends('spring'), 'spng')
    test(both_ends('Hello'), 'Helo')
    test(both_ends('a'), '')
    test(both_ends('xyz'), 'xyyz')


    print
    print 'fix_start'
    test(fix_start('babble'), 'ba**le')
    test(fix_start('aardvark'), 'a*rdv*rk')
    test(fix_start('google'), 'goo*le')
    test(fix_start('donut'), 'donut')

    print; print 'mix_up'
    test(mix_up('mix', 'pod'), 'pox mid')
    test(mix_up('dog', 'dinner'), 'dig donner')
    test(mix_up('gnash', 'sport'), 'spash gnort')
    test(mix_up('pezzy', 'firm'), 'fizzy perm')


# Standard boilerplate to call the main() function.
if __name__ == '__main__':
    main()
