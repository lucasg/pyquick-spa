#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""
Mimic exercise -- ejercicio estrella opcional
Google's Python Class

Leer el archivo especificado en la linea de comandos.
Hace un llamada simple a split() sobre espacio en blanco para
obtener todas las palabras en el archivo. (i.e. s.split(' '))
En vez de leer el archivo linea por linea, es mas facil guardarlo en
una string gigante y separarlo (split) solo una vez.

Construir un diccionario "mimic" que mapee cada palabra que aparece en
el archivo con una lista de palabras que le siguen inmediatamente a esa
palabra en el archivo. La lista de palabras puede estar en cualquier
orden y deberia incluir duplicados. Asi por ejemplo la key "and"
tendria la lista ["then", "best", "then", "after", ...] con todas las
palabras que aparecieron despues de la palabra "and" en el texto.
Diremos que la string vacia es la que viene antes de la primer palabra
en el archivo.

Con el diccionario mimic, es facil construir texto al azar que imite
al texto original. Imprimir una palabra, luego busca que palabras
podrian venir despues y eleji una al azar como la siguiente palabra.

Usa la string vacia como la primera palabra antes de todo.
Si en algun momento nos trabamos con una palabra que no esta en el
diccionario, volve a la string vacia para mantener las cosas en
movimiento.

Nota: el modulo estandar de python 'random' incluye el metodo
random.choice(list) el cual elige un elemento al azar de una lista
no vacia.

Por diversion, pasale tu programa a si mismo como input.
Podria funcionar haciendo que ponga linebreaks ('\n') cerca de la
columna 70 para que el output se vea mejor.

"""

import random
import sys


def mimic_dict(filename):
    """Devolver un mimic dict mapeando cada palabra a una lista de las
    palabras que le siguen.
    """
    # +++your code here+++
    return


def print_mimic(mimic_dict, word):
    """Dado un diccionario mimic y una palabra de comienzo, imprimir
    200 palabras al azar.
    """
    # +++your code here+++
    return


# Provided main(), calls mimic_dict() and mimic()
def main():
    if len(sys.argv) != 2:
        print 'usage: ./mimic.py file-to-read'
        sys.exit(1)

    dict = mimic_dict(sys.argv[1])
    print_mimic(dict, '')


if __name__ == '__main__':
    main()
