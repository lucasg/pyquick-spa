#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/


# Additional basic list exercises


# D.
#
# Dada una lista de numeros, devolver una lista donde todos los
# elementos que sean adyacentes e iguales se hayan reducido a un solo
# elemento.
#
# i.e. [1, 2, 2, 3] returns [1, 2, 3].
#
# Se puede crear una nueva lista o modificar la pasada como argumento.
def remove_adjacent(nums):
    # +++your code here+++
    return


# E.
#
# Dada dos listas ordenadas de forma incremental, crear y devolver una
# lista con todos los elementos ordenados.
# Se puede modificar la lista pasada como argumento.
#
# Idealmente la solucion deberia tener costo lineal, haciendo una sola
# pasada por cada lista.
def linear_merge(list1, list2):
    # +++your code here+++
    return


# Note: the solution above is kind of cute, but unforunately list.pop(0)
# is not constant time with the standard python list implementation, so
# the above is not strictly linear time.
# An alternate approach uses pop(-1) to remove the endmost elements
# from each list, building a solution list which is backwards.
# Then use reversed() to put the result back in the correct order. That
# solution works in linear time, but is more ugly.


# Simple provided test() function used in main() to print
# what each function returns vs. what it's supposed to return.
def test(got, expected):
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
        print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


# Calls the above functions with interesting inputs.
def main():
    print 'remove_adjacent'
    test(remove_adjacent([1, 2, 2, 3]), [1, 2, 3])
    test(remove_adjacent([2, 2, 3, 3, 3]), [2, 3])
    test(remove_adjacent([]), [])

    print
    print 'linear_merge'
    test(linear_merge(['aa', 'xx', 'zz'], ['bb', 'cc']),
         ['aa', 'bb', 'cc', 'xx', 'zz'])
    test(linear_merge(['aa', 'xx'], ['bb', 'cc', 'zz']),
         ['aa', 'bb', 'cc', 'xx', 'zz'])
    test(linear_merge(['aa', 'aa'], ['aa', 'bb', 'bb']),
         ['aa', 'aa', 'aa', 'bb', 'bb'])


if __name__ == '__main__':
    main()
