#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""
Wordcount exercise
Google's Python class

El main() de abajo ya esta definido y completo. Llama a las funciones
print_words() y print_top() las cuales hay que implementar.

1. Para la opcion --count, implementa una funcion print_words(filename)
que cuente que tan seguido cada palabra aparece en el texto e imprima:
word1 count1
word2 count2

Imprimi la lista de arriba de forma ordenada por palabra (python
ordenara la puntuacion antes que las lettras -- eso esta bien).
Guarda todas las palabras como minusculas, de forma que 'The' y 'the'
cuenten como la misma palabra.

2. Para la opcion --topcount, implementar una funcion
print_top(filename) que es similar a print_words() pero que imprime
solo las 20 palabras mas comunes ordenadamente, de forma que la palabra
mas comun aparezca primero, luego siguiente mas comun, etc.

Usa str.split() (sin argumentos) para separar por espacios en blanco.

Workflow: NO escribir todo el programa de una. Armarlo hasta un punto
intermedio e imprimir por pantalla tus estructuras de datos y salir
inmediatamente usando sys.exit(0).
Una vez que esa version este funcionando seguir con la siguiente.

Opcional: defini una funcion auxiliar para evitar codigo duplicado
adentro de print_words() y print_top()

"""

import sys

# +++your code here+++
#
# Definir las funciones print_words(filename) y print_top(filename).
# Podes escribir una funcion auxiliar que lea un archivo y construya
# y devuelva un diccionario word/count para el.
# Luego print_words() y print_top() pueden solo llamar a la funcion
# auxiliar.


#####


# This basic command line argument parsing code is provided and
# calls the print_words() and print_top() functions which you must define.
def main():
    if len(sys.argv) != 3:
        print 'usage: ./wordcount.py {--count | --topcount} file'
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)
    else:
        print 'unknown option: ' + option
        sys.exit(1)

if __name__ == '__main__':
    main()
