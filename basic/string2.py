#!/usr/bin/python2.4 -tt

# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/


# Ejercicios adicionales sobre strings.


# D. verbing
#
# Dada una string si su largo es al menos 3, agregar 'ing' al final.
# A menos que ya termine en 'ing', en ese caso agregar 'ly'.
# Si el largo de la string es menor a 3, dejarla sin modificar.
# Devolver la string resultante.
def verbing(s):
    # +++your code here+++
    return


# E. not_bad
#
# Dada una string, encontrar la primera aparicion de la
# substring 'not' and 'bad'. Si 'bad' aparece seguida de 'not',
# reemplazar todo la 'not'...'bad' substring con 'good'.
# Devolver la string resultante.
#
# So 'This dinner is not that bad!' returns 'This dinner is good!'
def not_bad(s):
    # +++your code here+++
    return


# F. front_back
#
# Considerar dividir una string en dos mitades.
# Si el largo es par, la mitad del frente y la mitad de atras tiene la
# misma longitud. Si el largo es impar, diremos que el extra char va
# en la mitad del frente.
#
# i.e. 'abcde', the front half is 'abc', the back half is 'de'.
# Dadas 2 strings, a and b, devolver una string de la forma:
#  a-front + b-front + a-back + b-back
def front_back(a, b):
    # +++your code here+++
    return


# Simple provided test() function used in main() to print
# what each function returns vs. what it's supposed to return.
def test(got, expected):
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
        print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


# main() calls the above functions with interesting inputs,
# using the above test() to check if the result is correct or not.
def main():
    print 'verbing'
    test(verbing('hail'), 'hailing')
    test(verbing('swiming'), 'swimingly')
    test(verbing('do'), 'do')

    print
    print 'not_bad'
    test(not_bad('This movie is not so bad'), 'This movie is good')
    test(not_bad('This dinner is not that bad!'), 'This dinner is good!')
    test(not_bad('This tea is not hot'), 'This tea is not hot')
    test(not_bad("It's bad yet not"), "It's bad yet not")

    print
    print 'front_back'
    test(front_back('abcd', 'xy'), 'abxcdy')
    test(front_back('abcde', 'xyz'), 'abcxydez')
    test(front_back('Kitten', 'Donut'), 'KitDontenut')

if __name__ == '__main__':
    main()
