#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/


# Ejercicios basicos de listas.
# Rellenar el codigo faltante en las funciones de abajo.
# La funcion main() ya esta seteada para llamar a las funciones
# con algunos argumentos diferentes, imprimiendo OK para las
# funciones correctas.
#
# Run Tests: python list1.py


# A. match_ends
#
# Dada una lista de strings, devolver devolver un int con la cantidad
# de strings de largo >= 2 y el primer y ultimo char igual.
def match_ends(words):
    # +++your code here+++
    return


# B. front_x
#
# Dada una lista de strings, devolver una lista con las strings
# ordenadas alfabeticamente. Pero agrupar todas las que comienzen con
# 'x' al principio de la lista resultante.
#
# i.e. ['mix', 'xyz', 'apple', 'xanadu', 'aardvark'] devuelve
#   ['xanadu', 'xyz', 'aardvark', 'apple', 'mix']
#
# Pista: una forma de resolverlo es hacer 2 listas y ordenar cada una
# antes de combinarlas.
def front_x(words):
    # +++your code here+++
    return


# C. sort_last
#
# Dada una lista de tuplas no-vacias, devolver una lista ordenada de
# forma incremental por el ultimo elementa de cada tupla.
#
# i.e. [(1, 7), (1, 3), (3, 4, 5), (2, 2)] devuelve
#   [(2, 2), (1, 3), (3, 4, 5), (1, 7)]
#
# Pista: crear una funcion last(x) para extraer el ultimo elemento de
# cada tupla
def sort_last(tuples):
    # +++your code here+++
    return


# Simple provided test() function used in main() to print
# what each function returns vs. what it's supposed to return.
def test(got, expected):
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
        print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


# Calls the above functions with interesting inputs.
def main():
    print 'match_ends'
    test(match_ends(['aba', 'xyz', 'aa', 'x', 'bbb']), 3)
    test(match_ends(['', 'x', 'xy', 'xyx', 'xx']), 2)
    test(match_ends(['aaa', 'be', 'abc', 'hello']), 1)

    print
    print 'front_x'
    test(front_x(['bbb', 'ccc', 'axx', 'xzz', 'xaa']),
         ['xaa', 'xzz', 'axx', 'bbb', 'ccc'])
    test(front_x(['ccc', 'bbb', 'aaa', 'xcc', 'xaa']),
         ['xaa', 'xcc', 'aaa', 'bbb', 'ccc'])
    test(front_x(['mix', 'xyz', 'apple', 'xanadu', 'aardvark']),
         ['xanadu', 'xyz', 'aardvark', 'apple', 'mix'])

    print
    print 'sort_last'
    test(sort_last([(1, 3), (3, 2), (2, 1)]),
         [(2, 1), (3, 2), (1, 3)])
    test(sort_last([(2, 3), (1, 2), (3, 1)]),
         [(3, 1), (1, 2), (2, 3)])
    test(sort_last([(1, 7), (1, 3), (3, 4, 5), (2, 2)]),
         [(2, 2), (1, 3), (3, 4, 5), (1, 7)])


if __name__ == '__main__':
    main()
