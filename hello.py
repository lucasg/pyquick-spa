#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""
Un pequeno programa python para chequear que python este funcionando.
Proba corrar el siguiente programa desde la linea de comandos de la
siguiente forma:

  python hello.py
  python hello.py Alice

Eso deberia imprimir:

  Hello World -or- Hello Alice

Proba cambiar el 'Hello' a 'Howdy' y correlo de nuevo.
Una vez que este funcionando, estas listo para la clase -- podes
editar y correr codigo Python; ahora solo necesitas aprender Python!

"""

import sys


# Define una funcion main() que imprime un pequeno saludo.
def main():
    # Obtiene el nombre desde la linea de comnados, usando 'World' como
    # valor por defecto.
    if len(sys.argv) >= 2:
        name = sys.argv[1]
    else:
        name = 'World'
        print 'Hello', name


# La variable __name__ es establecida como '__main__' solo cuando se llama
# al codigo de forma ejecutable (i.e. python hello.py).
#
# Si este modulo es importado por otro modulo (i.e. import hello),
# entonces, la variable es establecida como el nombre del modulo importado.
# Es decir, __name__ == 'hello'
if __name__ == '__main__':
    main()
