#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re

"""
Baby Names exercise

Define la funcion extract_names() abjao y modifica main() para
llamarla.

Para escribir regex, es bueno incluir una copia del texto objetivo
para inspiracion.


Asi es como el html se ve en los archivos baby.html:
...
<h3 align="center">Popularity in 1990</h3>
....
<tr align="right"><td>1</td><td>Michael</td><td>Jessica</td>
<tr align="right"><td>2</td><td>Christopher</td><td>Ashley</td>
<tr align="right"><td>3</td><td>Matthew</td><td>Brittany</td>
...

Pasos sugeridos para el desarrollo:
 -Extraer el year e imprimirlo
 -Extraer los nombres y los numeros de ranking e imprimirlos
 -Guardar los nombres en un diccionario e imprimirlo
 -Armar la lista [year, 'name rank', ... ] e imprimirla
 -Arreglar main() para usar la lista extract_names

"""


def extract_names(filename):
    """
    Dado un nombre de archivo para baby.html devolver una lista
    comenzando con la cadena year seguida por las cadenas name-rank
    en orden alfabetico.
    ['2006', 'Aaliyah 91', 'Aaron 57', 'Abagail 895', ... ]
    """
    # +++ Tu codigo va aqui +++
    return


def main():
    # El parseo de la entrada por linea de comandos esta provisto.
    # Hacer una lista con los argumentos, omitiendo el primer elemento,
    # que es el nombre del mismo script.
    args = sys.argv[1:]

    if not args:
        print 'usage: [--summaryfile] file [file ...]'
        sys.exit(1)

    # Notar la opcion --sumary y removerla de los argumentos
    summary = False
    if args[0] == '--summaryfile':
        summary = True
        del args[0]

    #
    # +++ Tu codigo va aqui +++
    #
    # Por cada filename, obtener los nombre, luego imprimirlos
    # o bien escribirlo a un archivo de resumen (summaryfile).

if __name__ == '__main__':
    main()
